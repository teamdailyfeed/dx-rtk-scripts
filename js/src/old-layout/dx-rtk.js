var RTK_tags = ['RTK_qw1q','RTK_wiHk','RTK_ZIkz','RTK_qQCt','RTK_iquJ'];
var $ = jQuery;

var DX_DEBUG_MODE = true;

if (!DX_DEBUG_MODE) {
  console = console || {};
  console.log = function(){};
}

jQuery(document).ready(function() {
  container.on( 'load.infiniteScroll', function( event, response, path ) {
      document.dx_rtk_tracking.first_loaded = 0;
      dx_get_first_ad();
	});
  dx_populate_two_eager_ad_slots();
});

jQuery(window).scroll(function() {
  // Track the existing window position
  var scrollY = window.scrollY;

  if( document.dx_rtk_tracking.ad_id != undefined && document.dx_rtk_tracking.ad_id != "") {
    console.log('doc_ad_id = ' + document.dx_rtk_tracking.ad_id);
    var el_top = dx_fetch_element_offset(document.dx_rtk_tracking.ad_id);

    // Print the diff between the scroll height and the element position
    // reverse order since otherwise negative num
    console.log( scrollY - el_top );

    // If under 110px, log in console
    if ( ( scrollY - el_top ) > -900 ) {
      console.log('Bingo');

      // TODO: fix the check if those exist at all
      var next_two_ads = jQuery('.site-content div[id^="RTK_"]:not([data-loaded])').slice(0,4);
      console.log(next_two_ads);

      //var ad1 = next_two_ads[0];
      //var ad2 = next_two_ads[1];
      var ad5 = next_two_ads[3];

      var ad2DivMapping = [];
      var ads = [];

      next_two_ads.each(function(i,ad) {
        if( jQuery( ad ) != undefined ) {
//console.warn(jQuery(ad).prev().prev().prev().html(), jQuery(ad).attr('id') + '_' + jQuery(ad).prev('div').attr('id'))
if(!ad2DivMapping[jQuery(ad).attr('id')]) ad2DivMapping[jQuery(ad).attr('id')] = [];
          ad2DivMapping[jQuery(ad).attr('id')].push(jQuery(ad).attr('id') + '_' + jQuery(ad).prev('div').attr('id'));
          ads.push(jQuery(ad).attr('id'));
          jQuery(ad).attr('id', jQuery(ad).attr('id') + '_' + jQuery(ad).prev('div').attr('id'));
		  jQuery(ad).attr("data-loaded", "yes");
          console.log('===> ' + jQuery(ad).attr('id') + '    ===  ' + jQuery(ad).prev('div').attr('id'))
        }
      });
console.warn(ad2DivMapping )

      if( ads.length != 0 ) {
        // Eager load
                  console.log('===== || ==== ' + ad2DivMapping + ' ===== || ====')

		window.top.jitaJS.que.push(function(ads, ad2DivMapping) {
          console.log("-----> rendering ads " + ads + " from bids cache")

  for (var i in ad2DivMapping) {
    // obj.hasOwnProperty() is used to filter out properties from the object's prototype chain
    if (ad2DivMapping.hasOwnProperty(i)) {
     ad2DivMapping[i].forEach(function(value, key) { 
console.warn([i], {[i]:value})
		          window.top.jitaJS.rtk.refreshAdUnits([i], false,{[i]: value});

     });
    }
  }
        }.bind(this, ads,ad2DivMapping));
        dx_populate_two_eager_ad_slots();
      }

      document.dx_rtk_tracking.ad_id = jQuery(ad5).prev('div').attr('id');
    }
  }
});


function dx_fetch_element_offset( id ) {
  // Track the document top offset of the existing element 
  var target_ad_div = document.getElementById(id);
  var el_top = target_ad_div.offsetTop;

  return el_top;
}

function dx_populate_two_eager_ad_slots() { 
  window.top.rtkEagerAdUnitCodes = [];
  jQuery('.site-content div[id^="RTK_"]:not([data-loaded])').slice(0,4).each(function(index,adSlot) {
    console.log(adSlot)
	window.rtkEagerAdUnitCodes.push(jQuery(adSlot).attr('id'));
  });
}
