var RTK_tags = ['RTK_qw1q','RTK_wiHk','RTK_ZIkz','RTK_qQCt','RTK_iquJ'];
var $ = jQuery;
var init_skip = false, slice_offset = 4;

jQuery(document).ready(function() {
  container.on( 'load.infiniteScroll', function( event, response, path ) {
      document.dx_rtk_tracking.first_loaded = 0;
      //dx_get_first_ad();
	});
  dx_populate_two_eager_ad_slots();
});

jQuery(window).scroll(function() {
  // Track the existing window position
  var scrollY = window.scrollY;

  if( document.dx_rtk_tracking.ad_id != undefined && document.dx_rtk_tracking.ad_id != "") {
    dx_log('doc_ad_id = ' + document.dx_rtk_tracking.ad_id);
    var el_top = dx_fetch_element_offset(document.dx_rtk_tracking.ad_id);

    // Print the diff between the scroll height and the element position
    // reverse order since otherwise negative num
    dx_log( scrollY - el_top );

    // If under 110px, log in console
    if ( ( scrollY - el_top ) > 110 ) {
      dx_log('Bingo');

      // TODO: fix the check if those exist at all
      if ( init_skip ) { 
        slice_offset = 0;
      }
      var next_two_ads = jQuery('.site-content div[id^="RTK_"]:not([data-loaded])').slice(slice_offset,4);
      dx_log(next_two_ads);

      var ad1 = next_two_ads[0];
      var ad2 = next_two_ads[1];
      var ad5 = next_two_ads[3];

      var ad2DivMapping = [];
      var ads = [];

      next_two_ads.each(function(i,ad) {
        if( jQuery( ad ) != undefined ) {
//console.warn(jQuery(ad).prev().prev().prev().html(), jQuery(ad).attr('id') + '_' + jQuery(ad).prev('div').attr('id'))
if(!ad2DivMapping[jQuery(ad).attr('id')]) ad2DivMapping[jQuery(ad).attr('id')] = [];
          ad2DivMapping[jQuery(ad).attr('id')].push(jQuery(ad).attr('id') + '_' + jQuery(ad).prev('div').attr('id'));
          ads.push(jQuery(ad).attr('id'));
          jQuery(ad).attr('id', jQuery(ad).attr('id') + '_' + jQuery(ad).prev('div').attr('id'));
		  jQuery(ad).attr("data-loaded", "yes");
          dx_log('===> ' + jQuery(ad).attr('id') + '    ===  ' + jQuery(ad).prev('div').attr('id'))
        }
      });
dx_log( ad2DivMapping )
      init_skip = true;

      if( ads.length != 0 ) {
        // Eager load
                  dx_log('===== || ==== ' + ad2DivMapping + ' ===== || ====')

		window.top.jitaJS.que.push(function(ads, ad2DivMapping) {
          dx_log("-----> rendering ads " + ads + " from bids cache")

  for (var i in ad2DivMapping) {
    // obj.hasOwnProperty() is used to filter out properties from the object's prototype chain
    if (ad2DivMapping.hasOwnProperty(i)) {
     ad2DivMapping[i].forEach(function(value, key) { 
console.warn([i], {[i]:value})
		          window.top.jitaJS.rtk.refreshAdUnits([i], false,{[i]: value});

     });
    }
  }
        }.bind(this, ads,ad2DivMapping));
        dx_populate_two_eager_ad_slots();
      }

      document.dx_rtk_tracking.ad_id = jQuery(ad1).prev('div').attr('id');
    }
  } else {
    var $content_wrapper = jQuery('.article-content').last();
    var $track = $content_wrapper.find('div[id^="RTK_"]').first();
    document.dx_rtk_tracking.ad_id = jQuery($track).attr('id');
  }
});


function dx_fetch_element_offset( id ) {
  // Track the document top offset of the existing element 
  var target_ad_div = document.getElementById(id);
  var el_top = target_ad_div.offsetTop;

  return el_top;
}

function dx_populate_two_eager_ad_slots() { 
  window.top.rtkEagerAdUnitCodes = [];
  jQuery('.site-content div[id^="RTK_"]:not([data-loaded])').slice(0,4).each(function(index,adSlot) {
    dx_log(adSlot)
	window.rtkEagerAdUnitCodes.push(jQuery(adSlot).attr('id'));
  });
}
