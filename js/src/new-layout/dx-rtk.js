var $ = jQuery;

domReady(function() {
  // Empty the Eager Load array for next batch
  window.top.rtkEagerAdUnitCodes = [];

  // No need to iterate on each slide so just fetch from the first load
  jQuery('.alm-nextpage').last().find('div[id^="RTK_"]').each(function() {
    var $this = jQuery(this);
    var ad = $this.attr('id');

    window.top.rtkEagerAdUnitCodes.push(ad);
  });

  window.top.rtkEagerAdUnitCodes.push('RTK_Pu0O');
});

domReady(function() {
  jQuery.fn.almComplete = function(alm){
    var ads = ['RTK_Pu0O'];
    var ad2DivMapping = {};

    // Create the proper ad2DivMapping and ads array here :)
    jQuery('.alm-nextpage').last().find('div[id^="RTK_"]').each(function() {
      var $this = jQuery(this);
      var ad = $this.attr('id');
      var uid = $this.prev('div').attr('id');
      $this.attr('id', ad + "_" + uid);
      ads.push(ad);
      ad2DivMapping[ad] = ad + "_" + uid;
    });

    window.top.jitaJS.que.push(function(ads,ad2DivMapping) {
	dx_log("-----> rendering ads " + ads + " from bids cache");
	window.top.jitaJS.rtk.refreshAdUnits(ads, false, ad2DivMapping);
    }.bind(this,ads,ad2DivMapping));

    window.top.jitaJS.rtk.regenerateUPID();

    // Refresh right sticky ad
    /*window.top.jitaJS.que.push(function() {
	dx_log("----> refreshing right rail <----");
	window.top.jitaJS.rtk.refreshAdUnits(['RTK_Pu0O'], false, {});
    });*/

    // Refresh bottom sticky ad
    rtkJitaSticky.refresh();
  };
});


// Trigger a click on the ALM button automatically since IS doesn't work
jQuery(window).scroll(function() {
	var scrollY = window.scrollY;
	var trigger_pos = document.getElementsByClassName('alm-load-more-btn')[0].offsetTop;
	if( ( scrollY - trigger_pos ) > -750 ) {
		if( jQuery('.alm-load-more-btn').length > 0 ) {
         		jQuery('.alm-load-more-btn').get(0).click();
    		}
  	}
});

jQuery(function($) {
    var setClassHidden = true;
    var $articleContentObj = $('.article-content');
    var $stickySideAd = $('.sticky-side-ad');

    $(window).bind('scroll', function() {

        if($(window).scrollTop() >= $articleContentObj.offset().top + $articleContentObj.outerHeight() - window.innerHeight ) {

            // We have reached the end of the page, hide the ad
            // setClassHidden = true;
        } else {

            // We are above the end of the page, show the ad
            setClassHidden = false;

            if ( $(window).scrollTop() <= $articleContentObj.offset().top ) {

                // if above wrapper, do not show the ad.
                setClassHidden = true;
                dx_log($(window).scrollTop());
                dx_log($articleContentObj.offset().top);
            }
        }

        // Set the classes once per scroll unit
        if (true === setClassHidden) {
            $stickySideAd.addClass('is-hidden');
        } else {
            $stickySideAd.removeClass('is-hidden');
        }
    });
});
