var jitaJS = window.jitaJS || {};
jitaJS.que = jitaJS.que || [];

window.rtkEagerAdUnitCodes = [];

var urlParams = new URLSearchParams(window.location.search);

function dx_log(msg) {
    if ( urlParams.has('dx_debug') ) {
	console.log(msg);
    }
    return;
}

var domReady = function(callback) {
  document.readyState === "interactive" || document.readyState === "complete"
    ? callback()
    : document.addEventListener("DOMContentLoaded", callback);
};

window.top.addEventListener("jitaRendered", function() {
       	jitaJS.que.push(function() {
       		//dx_log(window.rtkEagerAdUnitCodes);
       		dx_log("<----- fetching new bids for ----->" + window.rtkEagerAdUnitCodes + " into bids cache");
       		jitaJS.rtk.refreshAdUnits(window.rtkEagerAdUnitCodes, true, {}, true);
       	});
}, false);
