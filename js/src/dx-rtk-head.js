var jitaJS = window.jitaJS || {};
jitaJS.que = jitaJS.que || [];

window.rtkLazyAdUnitCodes = ['RTK_wiHk','RTK_ZIkz','RTK_qQCt','RTK_iquJ'];

window.rtkEagerAdUnitCodes = [];

var urlParams = new URLSearchParams(window.location.search);

function dx_log(msg) {
    if ( urlParams.has('dx_debug') ) {
	console.log(msg);
    }
    return;
}

window.top.addEventListener("jitaRendered", function() {
        jitaJS.que.push(function() {
                //dx_log(window.rtkEagerAdUnitCodes);
                dx_log("<----- fetching new bids for ----->" + window.rtkEagerAdUnitCodes + " into bids cache");
                jitaJS.rtk.refreshAdUnits(window.rtkEagerAdUnitCodes, true, {}, true);
        });
}, false);

document.dx_rtk_tracking = {
    ad_id: '',
    first_loaded: 0
};


var domReady = function(callback) {
  document.readyState === "interactive" || document.readyState === "complete"
    ? callback()
    : document.addEventListener("DOMContentLoaded", callback);
};

/// DAILY FEED CO UK
function dx_get_first_ad() {
  domReady(function() {
    if (
      document.dx_rtk_tracking.first_loaded == 0 &&
      (document.dx_rtk_tracking.ad_id == "" ||
        document.dx_rtk_tracking.ad_id == undefined)
    ) {
      var checkingForAd = 0;
      // Index = 1 because there is another ad above the content
      var first_ad = document.querySelectorAll(
        '.site-content div[id^="RTK_"]:not([data-loaded="yes"])'
      )[1];
      if (typeof first_ad === "undefined") {
        return;
      }
      document.dx_rtk_tracking.ad_id = first_ad.previousElementSibling
        ? first_ad.previousElementSibling.id
        : "";
      var ad2DivMapping = {};
      var uId = first_ad.previousElementSibling
        ? first_ad.previousElementSibling.id
        : Math.random().toString(36).substring(7);
      var rtk_tag = [first_ad.id];
      ad2DivMapping[rtk_tag] = rtk_tag + "_" + uId;
      document.getElementById(rtk_tag).setAttribute("id", rtk_tag + "_" + uId);
      dx_log(first_ad, ad2DivMapping);

      window.top.jitaJS.que.push(
        function(rtk_tag, ad2DivMapping) {
          dx_log(
            "-----> rendering ads " + first_ad.id + " from bids cache"
          );
          window.top.jitaJS.rtk.refreshAdUnits(rtk_tag, true, ad2DivMapping);
        }.bind(this, rtk_tag, ad2DivMapping)
      );

      first_ad.setAttribute("data-loaded", "yes");
      document.querySelectorAll(
        '.site-content div[id^="RTK_"]:not([data-loaded="yes"])'
      )[0].setAttribute("data-loaded", "yes");


      document.dx_rtk_tracking.first_loaded = 1;
    }
  });
}

window.top.addEventListener('jitaLoaded', function (e) {
    //dx_get_first_ad();
}, false);
