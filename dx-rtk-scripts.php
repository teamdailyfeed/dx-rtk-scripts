<?php
/*
Plugin Name: DX RTK Scripts
Version: 1.0.0
Description: Enable/Disable this plugin to toggle the RTK integration
Author: DevriX
*/

if ( ! defined( 'DX_RTK_ASSETS_VER' ) ) {
	define( 'DX_RTK_ASSETS_VER', '2018-06-01-1826' );
}

if ( ! defined( 'DX_RTK_JS_DIR' ) ) {
	define( 'DX_RTK_JS_DIR', plugins_url( '/js/', __FILE__ ) );
}

if ( ! defined( 'DX_RTK_CSS_DIR' ) ) {
	define( 'DX_RTK_CSS_DIR', plugins_url( '/css/', __FILE__ ) );
}

if ( ! class_exists( 'DX_RTK_Scripts' ) ) :
class DX_RTK_Scripts {
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'dx_rtk_add_scripts' ) );
		add_action( 'dx_rtk_injection', array( $this, 'dx_rtk_add_head_scripts' ) );
		add_filter( 'the_content', array( $this, 'df_insert_ad_containers' ) );
		add_shortcode( 'dx_custom_adunit', array( $this, 'dx_custom_adunit_shortcode' ) );
	}

	public function dx_rtk_add_head_scripts() {
		if ( is_front_page() || is_home() || is_404() || is_archive() || is_search() )
			return;

		if ( $this->dx_tag_exists() ) {
			echo '<script type="text/javascript" src="' . DX_RTK_JS_DIR . 'src/new-layout/dx-rtk-head.js' . '"></script>';
			echo '<script type="text/javascript" src="//thor.rtk.io/Sgr4/jita_sticky.js" async defer></script>';
		} else {
			echo '<script type="text/javascript" src="' . DX_RTK_JS_DIR . 'src/old-layout/dx-rtk-head.js' . '"></script>';
		}
	}

	public function dx_rtk_add_scripts() {
		if ( is_front_page() || is_home() || is_404() || is_archive() || is_search() )
			return;

		if ( $this->dx_tag_exists() ) {
			wp_enqueue_script( 'dx-rtk-ads', DX_RTK_JS_DIR . 'src/new-layout/dx-rtk.js', array( 'jquery' ), DX_RTK_ASSETS_VER );
			wp_enqueue_style( 'dx-rtk-css', DX_RTK_CSS_DIR . 'src/dx-rtk-stylesheet.css', array(), DX_RTK_ASSETS_VER, false);
		} else {
			wp_enqueue_script( 'dx-rtk-ads', DX_RTK_JS_DIR . 'src/old-layout/dx-rtk.js', array( 'jquery' ), DX_RTK_ASSETS_VER );
		}
	}

	public function df_insert_ad_containers( $content ) {
		if ( is_home() || is_front_page() || is_404() || is_archive() || is_search() )
        		return $content;

		$needle = '<div id="RTK_';
		$offset = strpos( $content, 'class="container"' );
		while ( ( $pos = strpos( $content, $needle, $offset ) ) !== FALSE ) {
			$offset = $pos + 45;
			$uid = uniqid();
			$content = substr_replace( $content, '<div id="' . $uid . '"></div>', $pos, 0 );
		}

		return $content;
	}

	public function dx_tag_exists() {
		return has_tag( 'infinitescroll' );
	}

	public function dx_custom_adunit_shortcode( $atts ) {
		$a = shortcode_atts( array(
			'desktop_id'	=> '',
			'mobile_id'	=> '',
		), $atts );

		$uid = uniqid();

		if ( wp_is_mobile() ) {
			if ( ! empty( $a['mobile_id'] ) ) {
				return '<div id="' . $uid . '"></div><div id="' . $a['mobile_id'] . '" style="overflow: hidden"></div>';
			}
		} else {
			if ( ! empty( $a['desktop_id'] ) ) {
				return '<div id="' . $uid . '"></div><div id="' . $a['desktop_id'] . '" style="overflow: hidden"></div>';
			}
		}
		return '';
	}
}
$dx_rtk_scripts = new DX_RTK_Scripts();
endif;
